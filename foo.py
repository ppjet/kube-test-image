#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Maxime "Pepe_" Buquet <pep+code@bouah.net>
#
# Distributed under terms of the MIT license.

"""
    Dumb url to title converter
"""

from flask import Flask
app = Flask(__name__)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return '<!DOCTYPE html><html><head><title>%s</title><head><body>%s</body></html>' % (path, path)
