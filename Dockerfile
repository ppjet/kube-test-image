
FROM python:3.6-onbuild

EXPOSE 5000

ENV FLASK_APP=foo.py
CMD ["flask", "run"]
